from django.http import HttpResponse
from django.shortcuts import render, redirect

from story5.models import Matkul
from .forms import matkulform


# Create your views here.

def post_story5(request):
    if request.method == 'POST':
        form = matkulform(request.POST or None, request.FILES or None)
        response_data = {}
        if form.is_valid():
            response_data['nama'] = request.POST['nama']
            response_data['dosen'] = request.POST['dosen']
            response_data['jumlahsks'] = request.POST['jumlahsks']
            response_data['deskripsi'] = request.POST['deskripsi']
            response_data['semestertahun'] = request.POST['semestertahun']
            response_data['ruangan'] = request.POST['ruangan']

            data_matkul = Matkul(nama=response_data['nama'],
                                 dosen=response_data['dosen'],
                                 jumlahsks=response_data['jumlahsks'],
                                 deskripsi=response_data['deskripsi'],
                                 semestertahun=response_data['semestertahun'],
                                 ruangan=response_data['ruangan'],
                                 )
            data_matkul.save()
            return redirect('/story5/#lihat_jadwal')

        else:
            return redirect('/story5')
    else:
        return redirect('/story5')


def story5(request):
    form_tambah = matkulform()
    data = Matkul.objects.all()
    response = {
        'form_tambah': form_tambah,
        'data': data,
    }
    return render(request, 'story5.html', response)


def detail_jadwal(request, nama_matkul):
    data = Matkul.objects.filter(nama=nama_matkul)
    response = {
        'data': data,
    }
    return render(request, 'story5-2.html', response)


def delete_jadwal(request, nama_matkul):
    print(nama_matkul)
    data = Matkul.objects.filter(nama=nama_matkul)
    data.delete()
    return redirect('/story5')
