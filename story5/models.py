from django.db import models


# Create your models here.

class Matkul(models.Model):
    nama = models.CharField(max_length=30)
    dosen = models.CharField(max_length=30)
    jumlahsks = models.CharField(max_length=30)
    deskripsi = models.CharField(max_length=20)
    semestertahun = models.CharField(max_length=20)
    ruangan = models.CharField(max_length=20)

    def __str__(self):
        return self.nama
