from django.test import TestCase
from django.test import Client
from django.urls import resolve

from story6.models import Kegiatan, Peserta
from . import views


# Create your tests here.

class UnitTestForStory6(TestCase):
    def test_response_page(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)

    def test_template_used(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'story6.html')

    def test_func_page(self):
        found = resolve('/story6/')
        self.assertEqual(found.func, views.story6)

    def test_story6_save_kegiatan_a_POST_request(self):
        Client().post('/story6/post-kegiatan', data={'nama_kegiatan': 'Unit Test'})
        jumlah = Kegiatan.objects.filter(nama='Unit Test').count()
        self.assertEqual(jumlah, 1)

    def test_story6_save_peserta_a_POST_request(self):
        obj = Kegiatan.objects.create(nama='Unit Test2')
        Client().post('/story6/post-peserta/' + str(obj.id), data={'nama_peserta': 'Bejo'})
        jumlah = Peserta.objects.filter(nama='Bejo').count()
        self.assertEqual(jumlah, 1)

    def test_story6_model_kegiatan(self):
        Kegiatan.objects.create(nama='Test Kegiatan')
        kegiatan = Kegiatan.objects.get(nama='Test Kegiatan')
        self.assertEqual(str(kegiatan), 'Test Kegiatan')

    def test_story6_kegiatan_form_invalid(self):
        Client().post('/story6/post-kegiatan', data={})
        jumlah = Kegiatan.objects.filter(nama='Unit Test').count()
        self.assertEqual(jumlah, 0)