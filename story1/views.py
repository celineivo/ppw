from django.shortcuts import render
import datetime


# Create your views here.

def story1(request):
    return render(request, 'story1.html')


def homepage(request, timezone='0'):
    time = datetime.datetime.now() + datetime.timedelta(hours=int(timezone))
    return render(request, 'homepage.html', {'time': str(time)})
