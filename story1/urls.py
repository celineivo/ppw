from django.urls import path
from . import views

urlpatterns = [
    path('story1', views.story1),
    path('', views.homepage),
    path('<int:timezone>/', views.homepage)
]